package practica1;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Practica1 extends JFrame implements ActionListener {
    JTextField tf;
    
    Practica1() {
        this.setTitle("Practica 1");
        this.setSize(300, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLayout(new FlowLayout());
        JLabel lbl = new JLabel("Escribe un nombre");

        JTextField tf = new JTextField(20);
        JButton btn = new JButton("Saludar!");
        
        btn.addActionListener(this);

        this.add(lbl);
        this.add(btn);
        this.add(tf);

    }

    public static void main(String[] args) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
        //To change body of generated methods, choose Tools | Templates.
        JOptionPane.showMessageDialog(this, "Hola" + tf.getText());
    }
}